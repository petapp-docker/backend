package com.example.petproject;

import com.example.petproject.entity.*;
import com.example.petproject.repository.*;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Create some initial demo / mock data for the app.
 * The class may be deleted if you do not want the demo data anymore
 */
@Service
@AllArgsConstructor
public class DbInitialize implements CommandLineRunner {
    PasswordEncoder passwordEncoder;
    UserRepository userRepository;
    RoleRepository roleRepository;
    PetRepository petRepository;
    PetClinicRepository petClinicRepository;
    AddressRepository addressRepository;
    PhoneRepository phoneRepository;

    @Override
    public void run(String... args) throws Exception {
        // To delete all data from db when the program starts
        // use this kind of deleteAll() sentences w each repository:
        // petRepository.deleteAll();

        petRepository.saveAll(createPets());
        roleRepository.saveAll(createRoles());
        userRepository.saveAll(createUsers());
        petClinicRepository.saveAll(createPetClinics());
        addressRepository.saveAll(createAddresses());
        phoneRepository.saveAll(createPhones());

        Role roleUserToUpdate = roleRepository.findByAuthority("USER").orElse(null);
        Role roleAdminToUpdate = roleRepository.findByAuthority("ADMIN").orElse(null);

        User user1ToUpdate = userRepository.findByUsername("user1").orElse(null);
        User admin1ToUpdate = userRepository.findByUsername("admin1").orElse(null);

        Pet pet1ToUpdate = petRepository.findByName("Milli").orElse(null);
        Pet pet2ToUpdate = petRepository.findByName("Caesar").orElse(null);
        Pet pet3ToUpdate = petRepository.findByName("Roby").orElse(null);

        PetClinic petClinic1ToUpdate = petClinicRepository
                .findByPetClinicName("PetMed Clinic").orElse(null);
        PetClinic petClinic2ToUpdate = petClinicRepository
                .findByPetClinicName("Pet Cure").orElse(null);

        Address address1ToUpdate = addressRepository.findById(1L).orElseGet(() -> createAddresses().get(1));
        Address address2ToUpdate = addressRepository.findById(2L).orElseGet(() -> createAddresses().get(2));

        Phone phone1ToUpdate = phoneRepository.findById(1L).orElseGet(() -> createPhones().get(1));
        Phone phone2ToUpdate = phoneRepository.findById(2L).orElseGet(() -> createPhones().get(2));

        assert roleUserToUpdate != null;
        assert user1ToUpdate != null;
        assert admin1ToUpdate != null;
        assert roleAdminToUpdate != null;
        assert pet1ToUpdate != null;
        assert pet2ToUpdate != null;
        assert pet3ToUpdate != null;
        assert petClinic1ToUpdate != null;
        assert petClinic2ToUpdate != null;

        user1ToUpdate.setAuthorities(new HashSet<>(Set.of(roleUserToUpdate)));
        admin1ToUpdate.setAuthorities(new HashSet<>(Set.of(roleAdminToUpdate)));

        user1ToUpdate.setPets(new HashSet<>(Set.of(pet1ToUpdate, pet2ToUpdate)));
        admin1ToUpdate.setPets(new HashSet<>(Set.of(pet3ToUpdate)));

        user1ToUpdate.setPetClinics(new HashSet<>(Set.of(petClinic1ToUpdate, petClinic2ToUpdate)));
        petClinic1ToUpdate.setUsers(new HashSet<>(Set.of(user1ToUpdate)));
        petClinic2ToUpdate.setUsers(new HashSet<>(Set.of(user1ToUpdate)));
        petClinic1ToUpdate.setAddresses(new HashSet<>(Set.of(address1ToUpdate)));
        petClinic2ToUpdate.setAddresses(new HashSet<>(Set.of(address2ToUpdate)));
        petClinic1ToUpdate.setPhones(new HashSet<>(Set.of(phone1ToUpdate)));
        petClinic2ToUpdate.setPhones(new HashSet<>(Set.of(phone1ToUpdate, phone2ToUpdate)));

        pet1ToUpdate.setUser(user1ToUpdate);
        pet2ToUpdate.setUser(user1ToUpdate);
        pet3ToUpdate.setUser(admin1ToUpdate);

        userRepository.save(user1ToUpdate);
        userRepository.save(admin1ToUpdate);
        petRepository.save(pet1ToUpdate);
        petRepository.save(pet2ToUpdate);
        petRepository.save(pet3ToUpdate);



    }
    public List<Role> createRoles(){
        Role roleUser = new Role("USER");
        Role roleAdmin = new Role("ADMIN");
        return List.of(roleUser, roleAdmin);
    }
    public List<User> createUsers(){
        User userUser1 = new User("user1", passwordEncoder.encode("user"));
        User userAdmin1 = new User("admin1", passwordEncoder.encode("admin"));
        return List.of(userUser1, userAdmin1);
    }

    public List<Pet> createPets(){
        Pet pet1 = new Pet("Milli", "Cat");
        Pet pet2 = new Pet("Caesar", "Dog");
        Pet pet3 = new Pet("Roby", "Dog");
        return List.of(pet1, pet2, pet3);
    }

    public List<PetClinic> createPetClinics(){
        PetClinic petClinic1 = new PetClinic("PetMed Clinic");
        PetClinic petClinic2 = new PetClinic("Pet Cure");
        return List.of(petClinic1, petClinic2);
    }

    public List<Address> createAddresses(){
        Address address1 = new Address("Tallinn", "Metsa");
        Address address2 = new Address("Tallinn", "Karja");
        return List.of(address1, address2);
    }
    public List<Phone> createPhones(){
        Phone phone1 = new Phone("+372 55 555 555");
        Phone phone2 = new Phone("+372 55 000 000");
        return List.of(phone1, phone2);
    }

}
