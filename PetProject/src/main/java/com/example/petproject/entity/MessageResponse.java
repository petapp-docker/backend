package com.example.petproject.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

//Used in UserController, ImageDataController
@Getter
@Setter
@ToString
@AllArgsConstructor
public class MessageResponse {
    private String message;
    private String parameterString;
}


