package com.example.petproject.controller;

import com.example.petproject.entity.Pet;
import com.example.petproject.entity.PetDTO;
import com.example.petproject.entity.PetAndImageIdDTO;
import com.example.petproject.service.PetService;
import com.example.petproject.service.UserPetService;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * App user Rest API controller to see their own pets, add, update and delete
 */

@Transactional
@RestController
@AllArgsConstructor
@RequestMapping("/api/pets")
@CrossOrigin(origins = "http://localhost:4200")
public class PetPageController {

    @Qualifier("PetOwner")
    PetService<Pet> petOwnerService;
    UserPetService<PetDTO> userPetService;

    @GetMapping("")
    public List<PetDTO> getPets() {
        return userPetService.getAllPetsOfTheAuthenticatedUser();
    }

    @GetMapping(value = "/{id}")
    public Pet getPet(@PathVariable Long id) {
        return petOwnerService.getPet(id);
    }

    @PostMapping(value = "")
    public ResponseEntity<Void> addPet(@RequestBody PetAndImageIdDTO petAndImageIdDTO) {
        Pet pet = petAndImageIdDTO.getPet();
        Long imageId = petAndImageIdDTO.getImageId();
        petOwnerService.addPet(pet, imageId);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    //TODO:: update the response entity!
    @PutMapping(value = "/{id}")
    public ResponseEntity<Void> updatePet(@PathVariable Long id, @RequestBody Pet pet) {
        petOwnerService.updatePet(id, pet);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT); //TODO check how it works
    }
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> deletePet(@PathVariable Long id) {
        petOwnerService.deletePet(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }
}
