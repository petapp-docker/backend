package com.example.petproject.service;

import com.example.petproject.entity.User;
import com.example.petproject.entity.UserDTO;
import com.example.petproject.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * App admin actions to see users, add, update and delete
 */

@Service("AdminUserService") //tells which PetService to use and when
@AllArgsConstructor
public class AdminUserServiceImpl implements UserService<User> {

    private UserRepository userRepository;

    @Override
    public Optional<User> findUserByUsername(String username) {
        return userRepository.findByUsername(username); //Optional.empty();
    }

    @Override
    public void addUser(User user) {
        if(!userRepository.existsById(user.getId())) {
            userRepository.save(user);
        }
    }

    @Override
    public List<User> getUsers() {
        return userRepository.findAll();
    }

    @Override
    public List<UserDTO> getUsersDTO() {
        List <UserDTO> users = new ArrayList<>();
        userRepository.findAll()
                .forEach(user -> users.add(new UserDTO(
                        user.getId(),
                        user.getUsername()
                )));
        return users;
    }
}
