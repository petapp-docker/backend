package com.example.petproject.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Getter
@Setter
//@EqualsAndHashCode
@JsonIgnoreProperties(ignoreUnknown = true)
@Table(name = "user", uniqueConstraints = {
        @UniqueConstraint(columnNames = "username")})
public class User implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = 20)
    @Column(unique = true)
    private String username;

    @NotBlank
    @Size(max = 120)
    private String password;

    //TODO::change FetchType later to LAZY and see how it works
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable( name = "users_to_roles",
                joinColumns = {@JoinColumn(name="user_id")},
                inverseJoinColumns = {@JoinColumn(name="role_id")})
    private Set<Role> authorities;



    @ManyToMany(fetch = FetchType.LAZY,
                cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable( name = "users_to_petclinics",
                joinColumns = {@JoinColumn(name="user_id")},
                inverseJoinColumns = {@JoinColumn(name="pet_clinic_id")})
    private Set<PetClinic> petClinics = new HashSet<>();


    @OneToMany (mappedBy = "user",
                cascade = CascadeType.ALL,
                fetch = FetchType.LAZY)
    private Set<Pet> pets = new HashSet<>();


    public User() {
        super();
        this.authorities = new HashSet<Role>();
    }


    public User(Long id) {
        super();
        this.id = id;
    }

    public User(String username, String password) {
        super();
        this.username = username;
        this.password = password;
    }

    public User(Long id, String username, String password){
        super();
        this.id = id;
        this.username = username;
        this.password = password;
    }

    public User(String username, String password, Set<Role> authorities) {
        super();
        this.username = username;
        this.password = password;
        this.authorities = authorities;

    }
    public User(String username, String password, Set<Role> authorities, Set<Pet> pets) {
        super();
        this.username = username;
        this.password = password;
        this.authorities = authorities;
        this.pets = pets;
    }

    public User(Long id, String username, String password, Set<Role> authorities){
        super();
        this.id = id;
        this.username = username;
        this.password = password;
        this.authorities = authorities;
    }

    public User(Long id, String username, String password, Set<Role> authorities, Set<PetClinic> petClinics, Set<Pet> pets) {
        super();
        this.id = id;
        this.username = username;
        this.password = password;
        this.authorities = authorities;
        this.petClinics = petClinics;
        this.pets = pets;
    }

    @Override
    public String getUsername() {
        return username;
    }
    @Override
    public String getPassword() {
        return password;
    }


    @Override public boolean isAccountNonExpired() { return true; }
    @Override public boolean isAccountNonLocked() { return true; }
    @Override public boolean isCredentialsNonExpired() { return true; }
    @Override public boolean isEnabled() { return true; }
    @Override public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.authorities;
    }
    public void setAuthorities(Set<Role> authorities) {
        this.authorities = authorities;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
