package com.example.petproject.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.*;
import org.springframework.lang.NonNull;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * For json / jackson annotations, see i.e.:
 * https://fasterxml.github.io/jackson-annotations/javadoc/2.9/com/fasterxml/jackson/annotation/JsonIgnoreProperties.html
 * https://stackoverflow.com/questions/20119142/jackson-multiple-back-reference-properties-with-name-defaultreference
 * TODO:: Should add equals and hashCode methods to Pet and User classes, though
 */
@Getter
@Setter
@Entity
@NoArgsConstructor
//@EqualsAndHashCode
@AllArgsConstructor
@RequiredArgsConstructor
public class Pet {
    @Id
    @ToString.Exclude
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NonNull private String name;
    @NonNull private String petType;

    @ManyToOne
    @JsonBackReference(value="pet-owner")
    private User user;

    @OneToOne   (cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable  (name = "pet_image",
                joinColumns = {@JoinColumn(
                    name = "pet_id",
                    referencedColumnName = "id")},
                inverseJoinColumns = { @JoinColumn(
                    name = "image_id",
                    referencedColumnName = "id")})
    @JsonBackReference(value="pet.image")           //needed for login to work correctly
    private ImageData petImage;

    public Pet(@NonNull String name, @NonNull String petType, User user) {
        this.name = name;
        this.petType = petType;
        this.user = user;
    }

    public Pet(@NonNull String name, @NonNull String petType, ImageData petImage) {
        this.name = name;
        this.petType = petType;
        this.petImage = petImage;
    }

    public Pet(@NonNull String name, @NonNull String petType, User user, ImageData petImage) {
        this.name = name;
        this.petType = petType;
        this.user = user;
        this.petImage = petImage;
    }

    @Override
    public String toString() {
        return "Pet{" +
                "name='" + name + '\'' +
                ", petType='" + petType + '\'' +
                ", user=" + user +
                ", petImage=" + petImage +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return Objects.equals(id, pet.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}

