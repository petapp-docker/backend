package com.example.petproject.service;

import com.example.petproject.entity.Address;
import com.example.petproject.entity.PetClinic;
import com.example.petproject.entity.PetClinicDTO;
import com.example.petproject.entity.Phone;
import com.example.petproject.repository.PetClinicRepository;
import com.example.petproject.repository.UserRepository;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;

/**
 * Pet clinic's actions to see and update info
 */
@Service("PetClinic")
@AllArgsConstructor
public class PetClinicServiceImpl implements PetClinicService<PetClinicDTO> {
    private PetClinicRepository petClinicRepository;


    //clinic (by id, with name, address not fetched due to fetch-lazy type)
    @Override
    public PetClinic getClinic(Long id) {
        return petClinicRepository.findById(id).orElseThrow(() ->
                new ResponseStatusException(HttpStatus.NOT_FOUND, "Pet clinic not found!"));
    }

    //DTO with name, address
    @Override
    @Transactional
    public PetClinicDTO getPetClinic(Long id) {
        String name = getClinic(id).getPetClinicName();
        Set <Address> addresses = getClinic(id).getAddresses();
        Set <Phone> phones = getClinic(id).getPhones();  //06.03.24
        //06.03.24 return new PetClinicDTO(name, addresses);
        return new PetClinicDTO(name, addresses, phones);   //06.03.24
    }

    @Override
    @Transactional
    public List<PetClinicDTO> getPetClinics() {
        List<PetClinic> petClinics = petClinicRepository.findAll();
        List <PetClinicDTO> clinics = new ArrayList<>();
        //06.03.24 petClinics.forEach(petClinic -> clinics.add(petClinicDTO(petClinic)));
        petClinics.forEach(petClinic -> clinics.add(petClinicDTOWithPhone(petClinic))); //06.03.24
        return clinics;
    }

    //DTO with id, name, address
    private PetClinicDTO petClinicDTO(PetClinic clinic){
        return new PetClinicDTO(clinic.getId(),
                                clinic.getPetClinicName(),
                                clinic.getAddresses()
        );
    }
    //DTO with id, name, address, phone //06.03.24
    private PetClinicDTO petClinicDTOWithPhone(PetClinic clinic){
        return new PetClinicDTO(clinic.getId(),
                clinic.getPetClinicName(),
                clinic.getAddresses(),
                clinic.getPhones()
        );
    }
}
