package com.example.petproject.service;

import com.example.petproject.entity.ImageData;
import org.springframework.web.multipart.MultipartFile;

import java.util.stream.Stream;

public interface ImageDataService {

    ImageData saveImage(MultipartFile file, String dateString) throws Exception;

    ImageData getImage(Long id);
    Stream<ImageData> getAllFiles();
    ImageData getImageByName(String imageName);

    ImageData getImageByDate(String date);


}
