# PetProject

**The app to see your pet data.**

## Description
Spring Boot Back-End
- The second bigger practical project during SDA JavaRemoteee27. Project creation time 16.12-21.01.24.
- The idea is to design back-end in Spring Boot and front-end in Angular.

**Current state**

**NEW:**
- Add picture of your pet (user / admin)
- See Pet clinics (names, addresses: city + street, phones).

BACK-END:
- Uses Spring Security, Spring Data JPA, Spring Starter Web. DB: MySQL.
- Dummy pet data is auto-generated ( `./PetProject/src/main/java/com/example/petproject/DbInitializer.java`)
- Dummy users are auto-generated while running the app.
- For demo purpose two users have different roles (ADMIN, USER).
- Each user can access to its own pets data only.
- No auth needed: home, about, contact, login, register page.
- Users data is saved to DB.
- JWT OAuth2 token based login.
- Users can read, add, update, delete each of their own pet data to/from DB (`./PetProject/src/main/java/com/example/petproject/controller/PetPageController.java`)
- Admin role can manage all pets (see, add + connect to user, update, delete). CRUD
- Contains methods to add user data in DB and connect REST API (POST)
- Each new user that is added after initialization has role ROLE_USER by default.
- Contains methods to see pet clinicsm, save to DB and send oved Rest API to FE
- Methods to save pet pictures to database and send them over REST API

RUN SEPARATELY:
- you may test querying pets data from DB by i.e. **Postman** or **curl**: fetch, add, update, delete pet data from DB (MySql should be running+logged in)
- ROLE_USER login:
  - user1 / user
- ROLE_ADMIN login:
  - admin1 / admin
- http://localhost:8080/admin/pets
- Web-pages endpoints: `./PetProject/src/main/java/com/example/petproject/controller/PetPageController.java` 

![PetPageController_2_29.12.23.png](..%2FImages%2FPetPageController_2_29.12.23.png)

## Prerequisites to run the code
- Java
- Maven
- Project uses Spring Boot (Security, Data, Web, Validation) so it might be useful to install some IDE for handling it, e.g, Intellij Pro
- MySql DB

## Launch procedure
- git clone this repository locally
- verify that mysql is running on the target system
- copy the file `src/main/resources/application.properties.example` into `src/main/resources/application.properties` and provide appropriate options
- the database tables will be created automatically because of the `spring.datasource.url=jdbc:mysql://localhost:3306/petapp?useSSL=false&createDatabaseIfNotExist=true`
- launch the application, the entry point is: **com.example.petproject.PetProjectApplication.java**

![entrypoint.png](..%2FImages%2Fentrypoint.png)

## Feature list
- To be updated

## Technologies used
Created with :
- Java 21, JDK 21.0.1
- Maven
- Lombok
- Spring Boot (Security, Data, Web, Validation)

## Authors
Katlin Kalde and ...

