package com.example.petproject.service;

import com.example.petproject.entity.LoginResponseDTO;
import com.example.petproject.entity.Pet;
import com.example.petproject.entity.Role;
import com.example.petproject.entity.User;
import com.example.petproject.repository.RoleRepository;
import com.example.petproject.repository.UserRepository;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

//To register new users
//transactions that if sth fails, whole transaction is cancelled
@Service
@Transactional
@AllArgsConstructor
public class AuthenticationService {

    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private PasswordEncoder encoder;
    private AuthenticationManager authenticationManager;
    private TokenService tokenService;

    public User registerUser(String username, String password) {
        System.out.println(" in registerUser ");
        String encodedPassword = encoder.encode(password);
        System.out.println( "Encoded password " + encodedPassword );
        Role userRole = roleRepository.findByAuthority("USER").get();
        System.out.println("User role: " + userRole + " uname " + username + " pw " + password);
        Set<Role> roles = new HashSet<>();
        roles.add(userRole);
        System.out.println( "Roles are in registerUser " + roles );
        return userRepository.save(new User(username, encodedPassword, roles));
    }

    //it takes the auth manager, looks for username, pw,
    //makes sure they are proper, generates auth token and send it over TokenService
    //generates the token and spits it out
    // Each time someone logs in, it passes the uname, pw to auth service
    // and if uname+pw are correct,from userdetailservice it grabs the user
    // and if user is found, spits out new token and return user + token
    public LoginResponseDTO loginUser(String username, String password){
        try{
            //try authenticating, creatinbg a new auth obj:
            Authentication auth = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(username, password));
            String token = tokenService.generateJwt(auth);
            return new LoginResponseDTO(userRepository.findByUsername(username).get(), token);
        } catch (AuthenticationException e){
            return new LoginResponseDTO(null, "");
        }
    }
}
