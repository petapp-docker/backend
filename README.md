# The Pet App

**The app to see your pet data.**

**Back-end: Spring Boot. Database: MySQL. Front-end: Angular. Use `docker compose up` to run the database and backend**

**You'll find the instructions to run the app in your computer under the section below: "Setup the project in your computer"**

SEE VIDEOS:
<br>

- Video 3. Added admin features to see, add, edit, delete all pets data: https://youtu.be/-pNZnRaCOYY
- Video 2: JWT OAuth2 token-based login, user accounts, see, edit, add, delete your own pets: https://youtu.be/u5vWTMf-2_k
- Video 1 (initial version): https://youtu.be/9T_yGN2UKOA


## Description

The second bigger practical project during SDA JavaRemoteee27. Project creation time 16.12.23-...02.24.
(The App is at the development phase right now.)

- Add, query, update, delete your pets' data. Keep an eye on its activities.


## Current state

**NEW:**
- Added dockerized backend + database
- Added some error messages in admin Pet edit panel (FE) - e.g. image size too big etc.
- Admin can add pet when pet's image size is too big, however image is not added then, only pet's description
- Add picture of your pet (user / admin)
- See Pet clinics (names, addresses: city + street, phones). 

**When Spring BE & MySQL is running, then in Angular FE you can:**
- See home, about and contacts page (w/o login)
- Role based login and rights (admin, user)
- Admin role can manage all pets (see, add + connect to user, update, delete) 
- User role can see and manage their own pets
- Register a new user
- Login to see:
    - your pets info from database,
    - add new pets,
    - edit your pets data,
    - delete your pets data
- JWT OAuth2 token-based login

**Currently in development:**
- List of pet clinics with addresses, phones (clinics, phones, addresses in separate tables).
- You may see each pet clinic info 
- Insert "Add image" option to pet editing section as well. Delete image w/o deleting pet
- Change admin "Add pet" form so that "Add pet" button is not active when user is not selected
- Change some issues with querying user data (see that password will not be sent over web if not needed)

**FRONT-END** ( folder: FEAngularPetProject )
- Angular page has header menu for routing
- Pages have basic design
- Home page card components are reusable, created from scratch (*ngFor, property binding)
- About page card components are reusable, created w Angular Material (*ngFor, property binding)
- Contact page has a contact form //TODO:: implement sending e-mail
- Pet clinics page (see clinics and their info). No login needed.
- Pets page needs login
- You may register a new user
- REST API used to fetch, delete and add pets data (from MySql DB through BE) 
- Frontend login: users credentials are saved in DB and 
- while login saved in local storage (JWT OAuth2 token based login) //TODO:: tokens to HttpOnly cookie
- Two dummy (demo)users are initially created while running the app:
- ROLE_USER login:
  - user1 / user
- ROLE_ADMIN login:
  - admin1 / admin
- Login: http://localhost:4200/pets
- Credentials are encrypted in DB
- See `./PetProject/src/main/java/com/example/petproject/PetProjectApplication.java` for the dummy (demo)users
- See `./PetProject/src/main/java/com/example/petproject/config/SecurityConfig.java` for the user roles / rights
- Users, admins can access and manipulate their own pets data (CRUD).  
- Admin role can manage all pets (see, add + connect to user, update, delete). CRUD
- App has basic mobile-views (customized for different types of mobiles)

**BACK-END** ( folder: PetProject ):
- Uses Spring Security, Spring Data JPA, Spring Starter Web. DB: MySQL.
- Dummy pet data is auto-generated ( `./PetProject/src/main/java/com/example/petproject/DbInitializer.java`)
- Dummy users are auto-generated while running the app.
- For demo purpose two users have different roles (ADMIN, USER).
- Each user can access to its own pets data only.
- No auth needed: home, about, contact, login, register page.
- Users data is saved to DB.
- JWT OAuth2 token based login.
- Users can read, add, update, delete each of their own pet data to/from DB (`./PetProject/src/main/java/com/example/petproject/controller/PetPageController.java`)
- Admin role can manage all pets (see, add + connect to user, update, delete). CRUD
- Contains methods to add user data in DB and connect REST API (POST)
- Each new user that is added after initialization has role ROLE_USER by default.
- Contains methods to see pet clinicsm, save to DB and send oved Rest API to FE
- Methods to save pet pictures to database and send them over REST API

**DATABASE** 
- MySQL database has 7 main tables: pet, user, role, address, phones, image_data, pet_clinic and some junction tables,
- DB / tables are auto-generated while booting (see: `./PetProject/src/main/resources/application.properties.example`)
- The initial dummy data is saved to DB while booting (see DB connection info in application.properties.example)

![petapp_database_schema_07.03.24.drawio.png](Images%2Fpetapp_database_schema_07.03.24.drawio.png)
![Angular3_20.01.24.png](Images%2FAngular3_20.01.24.png)
![add_pet_admin_view_22.02.24.png](Images%2Fadd_pet_admin_view_22.02.24.png)

## Prerequisites to run the code

**FE:**
- Angular CLI: Node.js 18.18.2 and Angular 17.0.7 (previous than 17.0.3 and later than 17.0.7 Angular builds might not be compatible)

**BE:**
- Docker 

**or for development:**

- Java (current project is on Java 21 / JDK 21 )
- Maven
- Project uses Spring Boot (Security, Data, Web, Validation) so it might be useful to install some IDE for handling it, e.g, Intellij Pro 
- MySql DB

![database_26.01.24.png](Images%2Fdatabase_26.01.24.png)


## Setup the project in your computer

To run it you need:
- Angular CLI 17 (Node.js 18.18.2 + Angular 17.0.7)
- Docker 
- For development purpose: Java 21 / JDK 21 Maven with Spring Boot

### Git clone BE and FE:
  - `git clone https://gitlab.com/petapp-docker/backend.git`
  - `git clone https://gitlab.com/petapp-docker/frontend.git`

### Run Angular Frontend:

- Open folder /frontend/FEAngularPetProject
- Open cmd.exe or terminal in ./FEAngularPetProject folder and run `npm install`
- Open VSCode (or any other Editor/IDE)
- Open in VSCode the ./FEAngularPetProject folder
- Open terminal in VSCode and run `ng serve`
- Wait until project build has completed
- Open in browser: http://localhost:4200
- You should see the following:
  ![img.png](img.png)

### Run Spring Boot Backend on Docker

- Install Docker in your computer
- In the cloned ./backend folder rename **.env.example** to: **.env**
- Copy **.env** to the same folder where the **./backend** folder is (i.e. if it is **./Desktop/backend**, then copy **.env**  to Desktop)
- Leave another **.env** inside the **./backend** folder (as it was)
- Start Docker (or Docker desktop) in your computer
- Open **cmd.exe** (or **terminal**) inside the folder **./backend**
- Run `docker compose up` in cmd.exe
- Wait until the database and backend are built up (may take ca 1-3 min)
- During this time you will see sth similar:
  ![img_1.png](img_1.png)

- You will see twice something like that (as the app checks first if the database already exists (and as it does not, it will create it)
  ![img_2.png](img_2.png)

- When the dockerized backend and database are up, you will see sth like that in cmd.exe:

  ![img_3.png](img_3.png)

- The last line contains: **: select pc1_0.user_id.pc1_1.id.pc1_1.pet /…/ pet_clinic_id where pc1_0.user_id=?**
- Now as the database, BE and FE are all up, you may start using the PetApp on http://localhost:4200

### Run Spring Boot BE in your computer (for development purpose))

- You would need to rename the environment variables in ./backend/.env file according to your database options, especially the username and password.
- In case you want to change the Spring backend port (8080), then you would need to change the port number accordingly in `./frontend/FEAngularPetProject/src/environments` as well
- Check that MySQL is running
- Login to your MySQL client
- Run Spring backend: `./PetProject/src/main/java/com/example/petproject/PetProjectApplication.java`
- Load Maven project (if asked) and enable Lombok annotations
- MySQL DB schema (database) and its tables are created automatically when you run the Spring BE.

## Feature list

## Technologies used
- Created with [Angular CLI](https://github.com/angular/angular-cli) version 17.0.7 on Node.js 18.18.2 (LTS)
- Java 21, JDK 21.0.1
- Maven
- Lombok
- Spring Boot (Security, Data, Web, Validation)

## Files logic

### The files logic in Java - Spring BE project
(to be added)
### The files logic in Angular 17 FE project
(to be added)


## Authors
Katlin Kalde

## Screenshots

![Angular_about_20.01.24.png](Images%2FAngular_about_20.01.24.png)

![Angular_contact_20.01.24.png](Images%2FAngular_contact_20.01.24.png)

![Angular_login_3_6.01.24.png](Images%2FAngular_login_3_6.01.24.png)

![Angular_register_3_26.01.24.png](Images%2FAngular_register_3_26.01.24.png)

![Angular_register_4_26.01.24.png](Images%2FAngular_register_4_26.01.24.png)

![user_own_space_20.01.24.png](Images%2Fuser_own_space_20.01.24.png)

![Angular_tokens_localstorage_24.01.24.png](Images%2FAngular_tokens_localstorage_24.01.24.png)

![select_picture_22.02.24.png](Images%2Fselect_picture_22.02.24.png)

![petpictures_22.02.24.png](Images%2Fpetpictures_22.02.24.png)

![Angular_pets_6_31.01.24.png](Images%2FAngular_pets_6_31.01.24.png)

![Angular_edit_pets_3_2.02.24.png](Images%2FAngular_edit_pets_3_2.02.24.png)

![add_pet_admin_view_22.02.24.png](Images%2Fadd_pet_admin_view_22.02.24.png)

![Angular_pets_mobile_GS20_1_31.01.24.png](Images%2FAngular_pets_mobile_GS20_1_31.01.24.png)

![Angular_pets_mobile_SG8+_20.01.24.png](Images%2FAngular_pets_mobile_SG8%2B_20.01.24.png)

![petclinics_22.02.24.png](Images%2Fpetclinics_22.02.24.png)

![petclinic_info_22.02.24.png](Images%2Fpetclinic_info_22.02.24.png)

**Error message when trying to perform non-authorized actions:**


![Angular_role_based_authorization_06.01.24.png](Images%2FAngular_role_based_authorization_06.01.24.png)

![Angular_role_based_authorization_deleting_06.01.24.png](Images%2FAngular_role_based_authorization_deleting_06.01.24.png)

![Angular_role_based_authorization_editing_06.01.24.png](Images%2FAngular_role_based_authorization_editing_06.01.24.png)

![Angular_register_errors_1_1.01.24.png](Images%2FAngular_register_errors_1_1.01.24.png)

![Angular_pets_5_20.01.24.png](Images%2FAngular_pets_5_20.01.24.png)

**Admin view:**

![admin_page1_31.01.24.png](Images%2Fadmin_page1_31.01.24.png)

![admin_page3_03.02.24.png](Images%2Fadmin_page3_03.02.24.png)

![admin_pet_edit_page_3.2.24.png](Images%2Fadmin_pet_edit_page_3.2.24.png)

**Pet clinics**

![petclinics_7.03.24.png](Images%2Fpetclinics_7.03.24.png)

![petclinic_7.03.24.png](Images%2Fpetclinic_7.03.24.png)

**Some backend examples:**

![be_admin_pets_20.12.23.png](Images%2Fbe_admin_pets_20.12.23.png)

![java_backend_29.12.23.png](Images%2Fjava_backend_29.12.23.png)

![PetPageController_2_29.12.23.png](Images%2FPetPageController_2_29.12.23.png)




