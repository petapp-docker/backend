package com.example.petproject.service;

import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.jwt.JwtClaimsSet;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import org.springframework.security.oauth2.jwt.JwtEncoderParameters;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class TokenService {

    private JwtEncoder jwtEncoder;
    private JwtDecoder jwtDecoder;

    public String generateJwt(Authentication auth) {
        //snapshot of the time of issuing the token
        Instant now = Instant.now();

        // The method collects authorities - leaps through all authorities inside auth
        // auth - Authentication obj has roles from users,
        // map them to GrantedAuthority (Role implements GrantedAuthority)
        // gets the authority (user, admin), combines them into one single string
        String scope = auth.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.joining(" "));


        // this specific service issues the tokens itself
        // .subject - the person and auth.getName - username
        JwtClaimsSet claims = JwtClaimsSet.builder()
                .issuer("self")
                .issuedAt(now)
                .subject(auth.getName())
                .claim("roles", scope)
                .build();


        //we use jwt encoder to encode jwt token of which we gain information of
        //jwtencoderparameters from our claims list which is issued to subjects username
        //builder JwtClaimsSet claims builds these claims
        //Get token value splits out string value, which we pass back to user in FE
        return jwtEncoder.encode(JwtEncoderParameters.from(claims)).getTokenValue();
    }
}

