package com.example.petproject.service;

public interface AdminPetClinicService <T>{
    void addPetClinic(T t);
    void updatePetClinic(Long id, T t);
    void deletePetClinic(Long id);
}
