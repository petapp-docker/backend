package com.example.petproject.controller;

import com.example.petproject.entity.Pet;
import com.example.petproject.entity.PetClinic;
import com.example.petproject.entity.PetClinicDTO;
import com.example.petproject.service.PetClinicService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * App admin Rest API controller to add, update and delete clinics
 * TODO:: implement methods with endpoints, see also PetClinicController class
 */

@RestController
@AllArgsConstructor
@RequestMapping("/api/petclinics")
public class AdminPetClinicController {


}
