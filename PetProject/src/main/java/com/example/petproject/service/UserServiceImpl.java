package com.example.petproject.service;

import com.example.petproject.entity.PetClinic;
import com.example.petproject.entity.Role;
import com.example.petproject.entity.User;
import com.example.petproject.repository.PetClinicRepository;
import com.example.petproject.repository.RoleRepository;
import com.example.petproject.repository.UserRepository;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 *  Actions with users.
 *  The user returned by loadUserByUsername(...) must be an instance of UserDetails.
 *  This is a type used by Spring Security to check
 *  if the user is not locked or disabled by administrator etc.
 *  so the User or the wrapper class needs to impl. UserDetails interface.
 *  See https://babarowski.com/blog/mock-authentication-with-custom-userdetails/ and
 *  https://github.com/gustavoponce7/SpringSecurityUserDetailsService/blob/master/src/main/java/com/example/service/UserServiceImpl.java
 */
@AllArgsConstructor
@Service("UserService")
public class UserServiceImpl implements UserDetailsService, PetClinicService<PetClinic> {
    private final UserRepository userRepository;
    private PetClinicRepository petClinicRepository;

    // returned user by next method must be an instance of UserDetails.
    // This is a type used by Spring Security to check
    // if the user is not locked or disabled by administrator etc.
    // so User class or wrapper class needs to implement UserDetails interface
    @Override
    public UserDetails loadUserByUsername(String username) {
        System.out.println("In user service impl");
        return userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("user not valid"));
    }


    @Override
    public PetClinic getPetClinic(Long id) {
        return null;
    }



    @Override
    public List<PetClinic> getPetClinics() {
        List <PetClinic> clinics = new ArrayList<>();
        return petClinicRepository
                .findAll()
                .stream()
                .filter(clinic -> {
                    assert getAuthenticatedUser() != null;
                    return getAuthenticatedUser().getPetClinics().contains(clinic) ? clinics.add(clinic) : null;
                })
                .toList();
    }


    public User getAuthenticatedUser(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();
        return userRepository.findByUsername(login).orElse(null);
    }
}
