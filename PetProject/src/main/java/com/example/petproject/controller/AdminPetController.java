package com.example.petproject.controller;

import com.example.petproject.entity.Pet;
import com.example.petproject.entity.PetAndImageIdDTO;
import com.example.petproject.entity.PetAndUserDTO;
import com.example.petproject.entity.PetDTO;
import com.example.petproject.service.AdminPetService;
import com.example.petproject.service.PetService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * App admin Rest API controller to see pets, add, update and delete
 */

@RestController
@AllArgsConstructor
@RequestMapping("/api/admins")
@CrossOrigin("http://localhost:4200")				//delete it later, if not needed!
public class AdminPetController {

    @Qualifier("AdminPetService")
    PetService<Pet> petService;

    AdminPetService<PetDTO> adminPetService;


    @GetMapping("/pets/{id}")
    public Pet getPet(@PathVariable Long id){
        return petService.getPet(id);
    }


    @GetMapping("/pets")
    public List<PetDTO> getPets() {
        System.out.println("AdminPetController: getPets() in getPets, trying to send data");
        return adminPetService.getPetsWithTheirPicturesAndUserUsernames();
    }

    @PostMapping(value = "/pets")
    public ResponseEntity<Void> addPet(@RequestBody PetAndImageIdDTO petAndImageIdDTO) {
        Pet pet = petAndImageIdDTO.getPet();
        Long imageId = petAndImageIdDTO.getImageId();
        petService.addPet(pet, imageId);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/pets/{id}")
    public ResponseEntity<Void> deletePet(@PathVariable Long id) {
        petService.deletePet(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }

    @PutMapping(value = "/pets/{id}")
    public ResponseEntity<Void> updatePet(@PathVariable Long id, @RequestBody Pet pet) {
        petService.updatePet(id, pet);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT); //TODO test for possible errors
    }
}
