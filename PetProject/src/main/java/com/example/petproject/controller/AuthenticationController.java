package com.example.petproject.controller;

import com.example.petproject.entity.LoginResponseDTO;
import com.example.petproject.entity.RegistrationDTO;
import com.example.petproject.entity.User;
import com.example.petproject.service.AuthenticationService;
import com.example.petproject.service.UserServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("http://localhost:4200")
@RequestMapping("/api/auth")
@AllArgsConstructor
public class AuthenticationController {

    private AuthenticationService authenticationService;
    private UserServiceImpl userService;

    //error msg is checked in FE, registration.component.ts
    @PostMapping("/register")
    public User registerUser(@RequestBody RegistrationDTO body){
        return authenticationService.registerUser(body.getUsername(), body.getPassword());
    }

    // TODO:: create separate LoginDTO
    //  (here I use a simple version)
    @PostMapping("/login")
    public LoginResponseDTO loginUser(@RequestBody RegistrationDTO body){
        try{
            authenticationService.loginUser(body.getUsername(), body.getPassword());
        } catch(Exception e) {
            e.printStackTrace();
        }
        return authenticationService.loginUser(body.getUsername(), body.getPassword());
    }
}
