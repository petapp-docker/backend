package com.example.petproject.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.springframework.lang.NonNull;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@NoArgsConstructor
@RequiredArgsConstructor
public class Phone {
    @Id
    @ToString.Exclude
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull @NonNull
    private String phoneNumber;

    @JsonBackReference(value="pet-clinics-with-phones")
    @ManyToMany(mappedBy = "phones",
                fetch = FetchType.LAZY)
    private Set<PetClinic> petClinics = new HashSet<>();
}
