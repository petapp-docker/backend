package com.example.petproject.utility;

import java.security.KeyPair;
import java.security.KeyPairGenerator;

//using nimbus
public class KeyGeneratorUtility{
    public static KeyPair generateRsaKey(){
        KeyPair keyPair;

        try{
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
            keyPairGenerator.initialize(2048);
            keyPair = keyPairGenerator.generateKeyPair();
        } catch(Exception e){
            throw new IllegalStateException();
        }

        return keyPair;
    }
}