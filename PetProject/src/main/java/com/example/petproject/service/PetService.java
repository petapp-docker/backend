package com.example.petproject.service;

import com.example.petproject.entity.PetDTO;

import java.util.List;

public interface PetService<T> {
        T getPet(Long id);
        void addPet(T t, Long id);
        void updatePet(Long id, T t);
        void deletePet(Long id);

}


