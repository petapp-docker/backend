package com.example.petproject.entity;

import lombok.*;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class PetClinicDTO {
    private Long id;
    private String petClinicName;
    private Set<Address> petClinicAddresses = new HashSet<>();
    private Set<Phone> petClinicPhones = new HashSet<>();

    public PetClinicDTO(String petClinicName, Set<Address> petClinicAddresses) {
        this.petClinicName = petClinicName;
        this.petClinicAddresses = petClinicAddresses;
    }

    public PetClinicDTO(Long id, String petClinicName, Set<Address> petClinicAddresses) {
        this.id = id;
        this.petClinicName = petClinicName;
        this.petClinicAddresses = petClinicAddresses;
    }

    public PetClinicDTO(String petClinicName, Set<Address> petClinicAddresses, Set<Phone> petClinicPhones) {
        this.petClinicName = petClinicName;
        this.petClinicAddresses = petClinicAddresses;
        this.petClinicPhones = petClinicPhones;
    }

    public PetClinicDTO(Long id, String petClinicName, Set<Address> petClinicAddresses, Set<Phone> petClinicPhones) {
        this.id = id;
        this.petClinicName = petClinicName;
        this.petClinicAddresses = petClinicAddresses;
        this.petClinicPhones = petClinicPhones;
    }
}
