package com.example.petproject.service;

import com.example.petproject.entity.PetDTO;

import java.util.List;

public interface UserPetService <T>{
    List<T> getAllPetsOfTheAuthenticatedUser();
}
