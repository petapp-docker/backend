package com.example.petproject.repository;

import com.example.petproject.entity.PetClinic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PetClinicRepository extends JpaRepository<PetClinic, Long> {
    Optional<PetClinic> findByPetClinicName(String name);

}
