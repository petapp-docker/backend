package com.example.petproject.controller;


import com.example.petproject.entity.ImageData;
import com.example.petproject.entity.ImageResponse;
import com.example.petproject.entity.MessageResponse;
import com.example.petproject.service.ImageDataService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.List;


@RestController
@AllArgsConstructor
@RequestMapping("/files")
public class ImageDataController {
    ImageDataService imageDataService;

    @PostMapping( "/upload")
    public ResponseEntity<MessageResponse> uploadFile(@RequestParam("file") MultipartFile file,
                                                      @RequestParam("localDateTime") String dateString) {
        String message = "";
        try {
            var image = imageDataService.saveImage(file, dateString);
            var idString = String.valueOf(image.getId());
            //06.03.24 System.out.println("In ResponseEntity<MessageResponse> uploadFile(), id is: " + idString);
            message = "Uploaded the file successfully: " + file.getOriginalFilename() + ". This is id string: " + idString;

            return ResponseEntity.status(HttpStatus.OK).body(new MessageResponse(message, idString));
        } catch (Exception e) {
            message = "Could not upload the file: " + file.getOriginalFilename() + "!";
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new MessageResponse(message, ""));
        }
    }

    @GetMapping({"/images", "/images/"})
    public ResponseEntity<List<ImageResponse>> getListOfImages(){
        List<ImageResponse> files = imageDataService.getAllFiles().map(dbImage -> {
            String fileDownloadUri = ServletUriComponentsBuilder
                    .fromCurrentContextPath()
                    .path("/files/images/")
                    .path(String.valueOf(dbImage.getId()))
                    .toUriString();
            return new ImageResponse(
                    dbImage.getName(),
                    fileDownloadUri,
                    dbImage.getType(),
                    //06.03.24 String.valueOf(dbImage.isPictureAdded()),
                    dbImage.getLocalDateTime().toString(),
                    dbImage.getImageData().length);
        }).toList();
        return ResponseEntity.status(HttpStatus.OK).body(files);
    }


    @GetMapping("/images/{id}")
    public ResponseEntity<byte[]> getImageById(@PathVariable Long id){
        ImageData image = imageDataService.getImage(id);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + image.getName() + "\"")
                .body(image.getImageData());
    }

    @GetMapping("/images/image")
    public ResponseEntity<ImageResponse> getImageByDate(@RequestParam String date){
        ImageData img = imageDataService.getImageByDate(date);
        String fileDownloadUri = ServletUriComponentsBuilder
                .fromCurrentContextPath()
                .path("/files/images/")
                .path(String.valueOf(img.getId()))
                .toUriString();
        ImageResponse file = new ImageResponse(
                img.getName(),
                fileDownloadUri,
                img.getType(),
                //06.03.24 String.valueOf(img.isPictureAdded()),
                img.getLocalDateTime().toString(),
                img.getImageData().length);

        //06.03.24 System.out.println("Date getmapping ImageController getImageByDate " + img.getName() + " " + img.getLocalDateTime());
        //06.03.24 System.out.println("Date getmapping ImageController getImageByDate " + file);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + img.getName() + "\"")
                .body(file);
    }

}
