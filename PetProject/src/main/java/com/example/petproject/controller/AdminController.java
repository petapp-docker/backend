package com.example.petproject.controller;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/admins")
@CrossOrigin("http://localhost:4200")				//delete it later, if not needed!
public class AdminController {

    @GetMapping("/")
    public String helloAdminController(){
        return "Admin access level";
    }


}