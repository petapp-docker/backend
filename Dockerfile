# Stage 1: Build the JAR
FROM openjdk:21-jdk AS builder
WORKDIR /usr/app
COPY ${JAR_LOCATION} app.jar

# Copy the .env file:
COPY .env /usr/.env
COPY .env /usr/app/.env
COPY .env /.env

# Stage 2: Create the final image
FROM openjdk:21-jdk
WORKDIR /usr/app
COPY --from=builder /usr/app/app.jar ./
CMD ["java", "-jar", "app.jar"]