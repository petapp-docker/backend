package com.example.petproject.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import lombok.*;
import org.springframework.lang.NonNull;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

//TODO::To be added: phone, e-mail, users etc.

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PetClinic {
    @Id
    @ToString.Exclude
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NonNull private String petClinicName;



    @JsonBackReference(value="pet-clinic-users")
    @ManyToMany(mappedBy = "petClinics",
                fetch = FetchType.LAZY)
    private Set<User> users = new HashSet<>();


    @ManyToMany(fetch = FetchType.LAZY,
                cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable( name = "pet_clinics_addresses",
                joinColumns = {@JoinColumn(name="pet_clinic_id")},
                inverseJoinColumns = {@JoinColumn(name="address_id")})
    private Set<Address> addresses = new HashSet<>();


    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable( name = "pet_clinics_phones",
                joinColumns = {@JoinColumn(name="pet_clinic_id")},
                inverseJoinColumns = {@JoinColumn(name="phones_id")})
    private Set<Phone> phones = new HashSet<>();


    @Override
    public String toString() {
        return "PetClinic{" +
                "clinicName='" + petClinicName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PetClinic petClinic = (PetClinic) o;
        return Objects.equals(id, petClinic.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
