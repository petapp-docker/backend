package com.example.petproject.service;

import com.example.petproject.entity.User;
import com.example.petproject.entity.UserDTO;

import java.util.List;
import java.util.Optional;


public interface UserService<T> {
    public Optional<User> findUserByUsername(String username);
    public void addUser(User user);
    List<T> getUsers();

    List<UserDTO> getUsersDTO();
}
