package com.example.petproject.service;

import com.example.petproject.entity.*;
import com.example.petproject.repository.ImageDataRepository;
import com.example.petproject.repository.PetRepository;
import com.example.petproject.repository.UserRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.*;

@Service("PetOwner")
@NoArgsConstructor
public class PetServiceImpl implements PetService<Pet>, UserPetService<PetDTO>{

    private PetRepository petRepository;

    private ImageDataRepository imageDataRepository;

    private UserRepository userRepository;

    @Autowired
    public PetServiceImpl(PetRepository petRepository, ImageDataRepository imageDataRepository, UserRepository userRepository) {
        this.petRepository = petRepository;
        this.imageDataRepository = imageDataRepository;
        this.userRepository = userRepository;
    }

    //This part checks if user can see a particular pet
    //SecurityContextHolder.getContext().getAuthentication() - gives currently logged in user
    @Override
    public Pet getPet(Long petId) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();

        Pet pet = petRepository.findById(petId).get();
        User user = pet.getUser();

        if(login.equals(user.getUsername())){
            return petRepository.findById(petId).orElseThrow(() ->
                    new ResponseStatusException(HttpStatus.NOT_FOUND, "Pet not found"));
        } else {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "You are not allowed to access the data");
        }
    }

    //This part finds all user's pets
    @Override
    public List<PetDTO> getAllPetsOfTheAuthenticatedUser(){
        User authUser = authenticatedUser();
        List<Pet> pets = petRepository.findAll();
        List<Pet> userPets = getPetsOfAUser(pets, authUser);

        return userPets.stream()
                .map(this::getAPetWithItsPicture)
                .toList();

    }


    public PetDTO getAPetWithItsPicture(Pet pet){
        String fileDownloadUri;
        ImageData image = pet.getPetImage();
        PetDTO petDTO;

        //if image exists, return petDTO object with image,
        //if not, return petDTO object w/o image:
        if(image != null){
            fileDownloadUri = ServletUriComponentsBuilder
                    .fromCurrentContextPath()
                    .path("/files/images/")
                    .path(String.valueOf(image.getId()))
                    .toUriString();

            petDTO = new PetDTO(pet.getId(), pet.getName(), pet.getPetType(), new ImageResponse(
                    image.getName(),
                    fileDownloadUri,
                    image.getType(),
                    image.getImageData().length));
        } else {
            petDTO = new PetDTO(
                    pet.getId(),
                    pet.getName(),
                    pet.getPetType());
        }
        return petDTO;
    }

    public List<Pet> getPetsOfAUser(List<Pet> allPets, User authUser){
        List<Pet> userPets = new ArrayList<>();
        for(Pet i : allPets) {
            try {
                User user = i.getUser();
                if (authUser.getUsername().equals(user.getUsername())) { userPets.add(i); }
            } catch(Exception e){
                System.out.println(e.getMessage() + " "  + " in " + getClass());    //+ Arrays.toString(e.getStackTrace())
            }
        }
        return userPets;
    }


    @Override
    public void addPet(Pet pet, Long imageId) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();
        User user = userRepository.findByUsername(login).orElse(null);
        if(imageId == 0) {
            petRepository.save(new Pet(pet.getName(), pet.getPetType(), user));
        } else {
            ImageData image = imageDataRepository.findById(imageId).orElse(null);
            petRepository.save(new Pet(pet.getName(), pet.getPetType(), user, image));
        }
    }

    @Override
    public void updatePet(Long id, Pet pet) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();
        User user = userRepository.findByUsername(login).orElse(null);

        Optional<Pet> petToUpdate = petRepository.findById(id);
        petToUpdate
                .map(current -> {
                    current.setName(pet.getName());
                    current.setPetType(pet.getPetType());
                    current.setUser(user);
                    petRepository.save(current);
                    return new ResponseStatusException(HttpStatus.NO_CONTENT); //"Pet updated";
                }) .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Pet not found"));
    }

    @Override
    public void deletePet(Long id) {
        System.out.println("in PetServiceImpl!");
        if(petRepository.existsById(id)){
            petRepository.deleteById(id);
            //return new ResponseEntity<Void>(HttpStatus.NO_CONTENT); //TODO: add response
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Pet not found");
        }
    }

    public User authenticatedUser(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();
        return userRepository.findByUsername(login).orElse(null);
    }

}
