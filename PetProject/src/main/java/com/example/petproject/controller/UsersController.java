package com.example.petproject.controller;


import com.example.petproject.entity.PetClinic;
import com.example.petproject.repository.UserRepository;
import com.example.petproject.service.PetClinicService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/api/users")
public class UsersController {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    @Qualifier("UserService")
    private final PetClinicService<PetClinic> petClinicService;

    @GetMapping("/")
    public String helloUserController(){
        return "User access level";
    }


}
