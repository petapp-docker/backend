package com.example.petproject.service;

import com.example.petproject.entity.ImageData;
import com.example.petproject.repository.ImageDataRepository;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

@Service
@AllArgsConstructor
public class ImageDataServiceImpl implements ImageDataService {
    ImageDataRepository imageDataRepository;


    //Idea from: https://www.bezkoder.com/spring-boot-upload-file-database/
    @Override
    @Transactional
    public ImageData saveImage(MultipartFile file, String dateString) throws IOException {
        String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
        LocalDateTime localDateTime = LocalDateTime.parse(dateString);
        ImageData imageData = new ImageData(fileName, file.getContentType(), localDateTime, file.getBytes());
        return imageDataRepository.save(imageData);
    }



    @Override
    @Transactional
    public ImageData getImage(Long id) {
        Optional<ImageData> imageData = imageDataRepository.findById(id);

        return imageData.orElseThrow(() ->
                new ResponseStatusException(HttpStatus.NOT_FOUND, "Picture not found"));
    }

    @Override
    public Stream<ImageData> getAllFiles() {
        return imageDataRepository.findAll().stream();
    }

    @Override
    @Transactional
    public ImageData getImageByName(String imageName){
        Optional<ImageData> imageData = imageDataRepository.findByName(imageName);

        return imageData.orElseThrow(() ->
                new ResponseStatusException(HttpStatus.NOT_FOUND, "Picture not found"));
    }

    @Override
    @Transactional
    public ImageData getImageByDate(String dateString){
        LocalDateTime date = LocalDateTime.parse(dateString);
        Optional<ImageData> imageData = imageDataRepository.findByLocalDateTime(date);
        return imageData.orElseThrow(() ->
                new ResponseStatusException(HttpStatus.NOT_FOUND, "Picture not found"));
    }

}
